package us.olaru.cassandra.rest.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtTokenInterceptor extends HandlerInterceptorAdapter {

    static Logger log = LoggerFactory.getLogger(JwtTokenInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("Intercepting: {} {}", request.getMethod(), request.getRequestURI());
        return super.preHandle(request, response, handler);
    }
}
