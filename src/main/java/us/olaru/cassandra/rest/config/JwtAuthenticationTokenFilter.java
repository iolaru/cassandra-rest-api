package us.olaru.cassandra.rest.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import us.olaru.cassandra.rest.JwtTokenUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    public final static String TOKEN_HEADER = "x-auth-token";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader(this.TOKEN_HEADER);

        if (authToken == null) {
            authToken = request.getParameter(TOKEN_HEADER);
        }

        logger.info("Token intercepted: " + authToken);

        if (authToken != null) {
            String username = jwtTokenUtil.getUsernameFromToken(authToken);
            String userUUID = jwtTokenUtil.getUserIdFromToken(authToken);

            // validate the token and authenticate user
            if (userUUID != null) {
                if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    logger.info("Checking authentication for user: " + username);
                    UserDetails userDetails = this.userDetailsService.loadUserByUsername(userUUID);
                    if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        logger.info("Authenticated user [{}], setting security context", username);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }

                }
            }
        }

        chain.doFilter(request, response);
    }
}