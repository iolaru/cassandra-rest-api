package us.olaru.cassandra.rest.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    static Logger log = LoggerFactory.getLogger(WebConfig.class);

    @Autowired
    JwtTokenInterceptor jwtTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("*** Attaching interceptor: {}", jwtTokenInterceptor.getClass().getCanonicalName());
        registry.addInterceptor(jwtTokenInterceptor).addPathPatterns("/rest/*");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        CorsRegistration registration = registry.addMapping("/**").allowedMethods("PUT", "DELETE", "POST", "GET");
    }

}
