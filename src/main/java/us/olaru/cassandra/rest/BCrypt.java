package us.olaru.cassandra.rest;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCrypt {

    public static String hash(String input) {
        return new BCryptPasswordEncoder().encode(input);
    }

    public static Boolean match(String input, String hash) {
        return new BCryptPasswordEncoder().matches(input, hash);
    }

    public static Boolean noMatch(String input, String hash) {
        return !match(input, hash);
    }

}
