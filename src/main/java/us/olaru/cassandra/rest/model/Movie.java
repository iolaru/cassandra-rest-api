package us.olaru.cassandra.rest.model;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.util.Map;

@Table("movies")
public class Movie {

    @PrimaryKey()
    private String id;

    @Column("title")
    private String title;

    @Column("genre")
    private Map<String, String> genre;

    @Column("numberInStock")
    private Integer numberInStock;

    @Column("dailyRentalRate")
    private Integer dailyRentalRate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumberInStock() {
        return numberInStock;
    }

    public void setNumberInStock(Integer numberInStock) {
        this.numberInStock = numberInStock;
    }

    public Integer getDailyRentalRate() {
        return dailyRentalRate;
    }

    public void setDailyRentalRate(Integer dailyRentalRate) {
        this.dailyRentalRate = dailyRentalRate;
    }

    public Map<String, String> getGenre() {
        return genre;
    }

    public void setGenre(Map<String, String> genre) {
        this.genre = genre;
    }
}
