package us.olaru.cassandra.rest.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import us.olaru.cassandra.rest.model.User;
import us.olaru.cassandra.rest.model.UserByEmail;

@Repository
public interface UserByEmailRepository extends CassandraRepository<UserByEmail> {
}
