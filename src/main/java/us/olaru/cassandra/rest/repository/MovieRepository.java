package us.olaru.cassandra.rest.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import us.olaru.cassandra.rest.model.Movie;

@Repository
public interface MovieRepository extends CassandraRepository<Movie> {
}
