package us.olaru.cassandra.rest.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import us.olaru.cassandra.rest.model.User;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        Boolean isAdmin = user.getAdmin() != null && user.getAdmin();
        List<GrantedAuthority> authorities = isAdmin ? mapToGrantedAuthorities("ROLE_ADMIN", "ROLE_USER") : mapToGrantedAuthorities("ROLE_USER");
        return new JwtUser(user.getEmail(), user.getName(), user.getPassword(), authorities);
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(String ... userRoles) {
        return Stream.of(userRoles).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
}