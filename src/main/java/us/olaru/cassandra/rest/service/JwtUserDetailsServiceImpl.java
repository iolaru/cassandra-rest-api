package us.olaru.cassandra.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.support.BasicMapId;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import us.olaru.cassandra.rest.model.User;
import us.olaru.cassandra.rest.repository.UserRepository;
import us.olaru.cassandra.rest.security.JwtUser;
import us.olaru.cassandra.rest.security.JwtUserFactory;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public JwtUser loadUserByUsername(String userUUID) throws UsernameNotFoundException {
        User user = userRepository.findOne(BasicMapId.id("id", userUUID));
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with id '%s'.", userUUID));
        } else {
            return JwtUserFactory.create(user);
        }
    }
}
