package us.olaru.cassandra.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.olaru.cassandra.rest.BCrypt;
import us.olaru.cassandra.rest.JwtTokenUtil;
import us.olaru.cassandra.rest.model.User;
import us.olaru.cassandra.rest.repository.UserRepository;

import java.util.UUID;

@RestController
@RequestMapping("/rest/users")
public class UserController {

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    UserRepository userRepository;

    @PostMapping
    public ResponseEntity<String> register(@RequestBody RegistrationRequest registrationRequest) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setName(registrationRequest.getName());
        user.setEmail(registrationRequest.getEmail());
        user.setPassword(BCrypt.hash(registrationRequest.getPassword()));
        userRepository.save(user);
        String token = jwtTokenUtil.generateToken(user.getId(), user.getEmail(), user.getName(), false);
        return ResponseEntity.ok().header("x-auth-token", token).header("access-control-expose-headers", "x-auth-token").build();
    }

}
