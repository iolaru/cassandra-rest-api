package us.olaru.cassandra.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.support.BasicMapId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import us.olaru.cassandra.rest.Tap;
import us.olaru.cassandra.rest.model.Genre;
import us.olaru.cassandra.rest.model.Movie;
import us.olaru.cassandra.rest.repository.GenreRepository;
import us.olaru.cassandra.rest.repository.MovieRepository;

import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping("/rest/movies")
public class MovieController {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    GenreRepository genreRepository;

    @GetMapping
    public Iterable<Movie> list() {
        return movieRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable("id") String id) {
        Movie movie = movieRepository.findOne(BasicMapId.id("id", id));
        if (movie == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(movie);
    }

    @PostMapping
    public ResponseEntity<Movie> createMovie(@RequestBody MovieRequest movieRequest) {
        Movie movie = new Movie();
        movie.setId(UUID.randomUUID().toString());
        updateMovieDetails(movie, movieRequest);
        return ResponseEntity.ok(movieRepository.save(movie));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable("id") String id, @RequestBody MovieRequest movieRequest) {
        Movie movie = movieRepository.findOne(BasicMapId.id("id", id));
        if (movie == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        updateMovieDetails(movie, movieRequest);
        return ResponseEntity.ok(movieRepository.save(movie));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteMovie(@PathVariable("id") String id) {
        Movie movie = movieRepository.findOne(BasicMapId.id("id", id));
        if (movie == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        movieRepository.delete(movie);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    private void updateMovieDetails(Movie movie, MovieRequest movieRequest) {
        Genre genre = genreRepository.findOne(BasicMapId.id("id", movieRequest.getGenreId()));
        movie.setGenre(Tap.tap(new HashMap<>(2), hm -> { hm.put("id", genre.getId()); hm.put("name", genre.getName()); }));
        movie.setTitle(movieRequest.getTitle());
        movie.setDailyRentalRate(movieRequest.getDailyRentalRate());
        movie.setNumberInStock(movieRequest.getNumberInStock());
    }

}
