package us.olaru.cassandra.rest.api;

public class MovieRequest {

    private String title;
    private String genreId;
    private Integer numberInStock;
    private Integer dailyRentalRate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public Integer getNumberInStock() {
        return numberInStock;
    }

    public void setNumberInStock(Integer numberInStock) {
        this.numberInStock = numberInStock;
    }

    public Integer getDailyRentalRate() {
        return dailyRentalRate;
    }

    public void setDailyRentalRate(Integer dailyRentalRate) {
        this.dailyRentalRate = dailyRentalRate;
    }
}
