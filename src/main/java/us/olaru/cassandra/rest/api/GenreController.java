package us.olaru.cassandra.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.olaru.cassandra.rest.model.Genre;
import us.olaru.cassandra.rest.repository.GenreRepository;

@RestController
@RequestMapping("/rest/genres")
public class GenreController {

    @Autowired
    GenreRepository genreRepository;

    @GetMapping
    public Iterable<Genre> list() {
        return genreRepository.findAll();
    }

}
