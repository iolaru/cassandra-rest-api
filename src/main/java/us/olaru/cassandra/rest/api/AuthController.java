package us.olaru.cassandra.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.support.BasicMapId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.olaru.cassandra.rest.BCrypt;
import us.olaru.cassandra.rest.JwtTokenUtil;
import us.olaru.cassandra.rest.model.UserByEmail;
import us.olaru.cassandra.rest.repository.UserByEmailRepository;
import us.olaru.cassandra.rest.repository.UserRepository;

@RestController
@RequestMapping("/rest/auth")
public class AuthController {

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserByEmailRepository userByEmailRepository;

    @PostMapping
    public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest) {
        UserByEmail userByEmail = userByEmailRepository.findOne(BasicMapId.id("email", loginRequest.getEmail()));

        if (userByEmail == null) return notFound();

        boolean passwordMatch = BCrypt.match(loginRequest.getPassword(), userByEmail.getPassword());

        if (passwordMatch) {
            boolean isAdmin = userByEmail.getAdmin() != null && userByEmail.getAdmin();
            return ResponseEntity.ok(jwtTokenUtil.generateToken(userByEmail.getId(), userByEmail.getEmail(), userByEmail.getName(), isAdmin));
        } else {
            return notFound();
        }
    }

    private ResponseEntity<String> notFound() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid email or password.");
    }

}
