## Spring Data Cassandra

This Spring Boot API project is using Apache Cassandra (3.11.5) as the database
http://mirrors.ocf.berkeley.edu/apache/cassandra/3.11.5/apache-cassandra-3.11.5-bin.tar.gz


- Create a folder named `playground`
```
mkdir playground
```

- Download apache cassandra
```
cd playground
wget http://mirrors.ocf.berkeley.edu/apache/cassandra/3.11.5/apache-cassandra-3.11.5-bin.tar.gz
tar -xvzf ./apache-cassandra*.tar.gz
```

- Run apache cassandra
```
./apache-cassandra-3.11.5/bin/cassandra -f
```
keep `-f` switch if you want to monitor the logs

- Download and build cassandra-rest-api code
```
git clone git@bitbucket.org:iolaru/cassandra-rest-api.git
cd cassandra-rest-api
./gradlew clean build -i
```

- import init data into cassandra
```
../apache-cassandra-3.11.5/bin/cqlsh < ./01.cql
```

- check `cassandra.properties` file, by default spring boot app connects to cassandra on `127.0.0.1` and port `9042` 
```
cassandra.contactpoints=127.0.0.1
cassandra.port=9042
cassandra.keyspace=vidly
```

- start the api
```
./gradlew bootRun
```

- check the api
```
curl -i http://127.0.0.1:8080/rest/genres/
curl -i http://127.0.0.1:8080/rest/movies/
```

